
# InfluxDBLineProtocolParser

This project is for parsing and modifying InfluxDB 
[Line Protocol](https://docs.influxdata.com/influxdb/cloud/reference/syntax/line-protocol/)

At this point the Main-program does only calculate the frequencies of different
MAC-addresses, but the parser itself is capable of also modifying the Line 
Protocol lines. This program also utilizes Rust's ability for easy and safe
concurrency.

TODO:
* CLI arguments for different modes of running the program:
    (statistics, db modification)
* writing modified database back to a file
* check why program uses the more RAM the larger the input file is


## Motivation

I had a 22 GB database dump (in Line Protocol format, as a database it is
about 600MB) from
[RuuviCollector](https://github.com/Scrin/RuuviCollector) but I did not know 
in the beginning, that RuuviTags could be annotated with names as a tag field.
One RuuviTag also broke so now I have two different MAC addresses from 
RuuviTags on my balcony.

I also had to move the whole database from InfluxDB 1.7 to 2.1 so I had to
create an intermediate LineProtocol dump and as I found out it is plain text
file, I wanted to do an exerciese and create a software in Rust for parsing the
file and modifying it. 

Little did I know how complicated format it actually is. This parser is not 
able to parse every possible InfluxDB Line Protocol file, because escaping
characters is a pain to implement. If you are concerned, the comments in code
explain more about this and which situations are or aren't supported. If you
don't use special characters which must be escaped (like '='), you are fine.


## Line Protocol

Example of InfluxDB line protocol:

``` 
$ head ruuvi.csv
# INFLUXDB EXPORT: 1677-09-21T00:12:43Z - 2262-04-11T23:47:16Z
# DDL
CREATE DATABASE ruuvi_new WITH NAME autogen
# DML
# CONTEXT-DATABASE:ruuvi_new
# CONTEXT-RETENTION-POLICY:autogen
# writing tsm data
ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 absoluteHumidity=3.356006433735946 1543795206029000000
ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 absoluteHumidity=3.356006433735946 1543795217081000000
ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 absoluteHumidity=3.356006433735946 1543795228155000000
```

## Multi-threading

Main-thread is responsible for parsing the input file. First Main-thread checks
how many threads should be allocated, then it creates the Worker-threads
and the Collector-thread.

For example, my laptop has 8 logical cores so the Main-thread will create 8
threads. Then it will read the file line-by-line and distribute the line
-strings in a round-robin manner to all the Worker-threads using
std::sync::mpsc channels (each thread has its own channel). After all lines are
sent, a WorkerThreadMessage::NoMoreLines enum is sent to the Worker-threads.

The Worker-threads will then parse the received line into a Line-struct and 
send the resulting mac-address to the collector thread. After receiving the
NoMoreLines-enum, they in turn will send a NoMoreMACs-message to the Collector-
thread. Worker-threads communicate to Collector-thread using a single mpsc-
channel, with N inputs and 1 output.

The Collector-thread will accumulate all received MAC-addressess and update a
BTreeMap based on them. Main-thread will join the Collector-thread (since Main
will finish sooner reading the input file). 

After the Collector-thread has received NoMoreMACs-enums as many as there are
threads in the system running, it will pass the BTreeMap back to Main-thread as
a return value which will print it.

### Concurrency diagram
```
+-----------------------------------------------------------------------+
|                                                                       |
|  Main-thread reads input file                                         |
|                                                                       |
+-----------------------------------------------------------------------+
    ||       ||       ||       ||       ||       ||       ||       ||         
    ||       ||  N communication channels to N threads    ||       ||       
    ||       ||  Main-thread sends lines based on Round   ||       ||
    ||       ||  Robin to the N Worker-threads via MPSC   ||       ||
    ||       ||  -channels     ||       ||       ||       ||       ||
    ||       ||       ||       ||       ||       ||       ||       ||         
    \/       \/       \/       \/       \/       \/       \/       \/         
+--------+--------+--------+--------+--------+--------+--------+--------+
| Worker | Worker | Worker | Worker | Worker | Worker | Worker | Worker | 
| thread | thread | thread | thread | thread | thread | thread | thread | 
|   0    |   1    |   2    |   3    |   4    |   5    |  ...   |   N    |
+--------+--------+--------+--------+--------+--------+--------+--------+
    ||       ||       ||       ||       ||       ||       ||       ||         
    ||       ||       ||       ||       ||       ||       ||       ||         
    ||       ||       ||       ||       ||       ||       ||       ||         
    \===============================================================/
             ||
             || One MPSC channel: N producers, 1 consumer
             ||
             \/
+-----------------------------------------------------------------------+
|                                                                       |
|  Collector-thread                                                     |
|                                                                       |
+-----------------------------------------------------------------------+
             ||
             || Collector-thread, once received 'finished' 
             || from all Worker-threads, sends result 
             || (hashmap and other variables) back to 
             || Main-thread as return value
             ||
             \/
+-----------------------------------------------------------------------+
|                                                                       |
|  Main-thread prints the received information                          |
|                                                                       |
+-----------------------------------------------------------------------+
```

# TODO: 
    - write-trait for Line

