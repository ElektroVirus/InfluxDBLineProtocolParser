mod influx_line_parser;

use std::thread;
use std::sync::mpsc;

use std::fs::File;
// use std::io::{BufRead, BufReader, Write, BufWriter};
use std::io::{BufRead, BufReader};
use std::collections::BTreeMap;
use phf::phf_map;

/*
 * Suoriusaika debug-tilassa:   11:18 -> 11:44 (26 min)
 * Suoriusaika release-tilassa: 11:46 -> 11:51 (5 min)
 *
 * */

// Lookup table for my RuuviTags names and mac addresses
static RUUVITAGS: phf::Map<&'static str, &'static str> = phf_map! {
    "F896B44D95A5" => "Metalliluola",
    "E53C0EF7C341" => "Terassi",
    "F60D8A643276" => "Miesluola",
    "EAD61D7CCF5D" => "Parveke",
    "C0CFD2A1F6ED" => "Parveke_vanha",
    "CFD39F634DC1" => "Aroniaviini_1"
};

// Message to be sent from Main thread to Worker threads
enum WorkerThreadMessage {
    Line(String),
    NoMoreLines,
}

// Message to be sent from Worker thread to Collector thread
enum CollectorThreadMessage {
    Mac(String),
    Failure,
    NoMoreMacs(String)
}

// Message to be sent from Collector thread to Main thread
struct RetMessage {
    mac_map: BTreeMap<String, u32>,
    total_measurements: u32,
    invalid_measurements: u32,
    invalid_lines: u32,
}

#[allow(dead_code)]
struct WorkerThread {
    thread_handle:      thread::JoinHandle<()>,
    input_channel_tx:   mpsc::Sender<WorkerThreadMessage>,
}


fn main() {

    const INFILENAME: &str = "ruuvi.csv";
    // const OUTFILENAME: &str = "ruuvi_modified.csv";

    let infile_opt = File::open(INFILENAME);
    if let Err(_e) = infile_opt {
        println!("File '{}' not found.", INFILENAME);
        return;
    }

    let reader = BufReader::new(infile_opt.unwrap());

    // let outfile = File::create(OUTFILENAME).unwrap();
    // let mut writer = BufWriter::new(outfile);

    // Channel to communicate between parsing threads and collector-thread
    let (tx_collector, rx_collector): 
            ( mpsc::Sender<CollectorThreadMessage>, 
            mpsc::Receiver<CollectorThreadMessage> ) 
                = mpsc::channel();

    // Check how many worker-threads to create
    let thread_count = thread::available_parallelism()
        .map(|value| value.get())
        .unwrap_or(1);

    let collector_thread_handle = spawn_collector_thread(rx_collector, thread_count);

    let mut worker_thread_vector: Vec<WorkerThread> = Vec::new();

    //************************************************************
    // Create worker-threads
    //************************************************************
    for i in 0 .. thread_count {
        // println!("Constructing thread #{}", i);

        let (tx_worker, rx_worker): (mpsc::Sender<WorkerThreadMessage>, 
                       mpsc::Receiver<WorkerThreadMessage>) 
                       = mpsc::channel();

        let thread_name = format!("Worker_{}", i);
        let tx_worker_collector = tx_collector.clone();
        let thread_handle_res = thread::Builder::new().name(thread_name.clone()).spawn(move || {
        let tx_new = tx_worker_collector.clone();
            loop {
                let input = rx_worker.recv().unwrap();
                match input {
                    WorkerThreadMessage::NoMoreLines=> break,
                    WorkerThreadMessage::Line(line) => process_line(&line, &tx_new),
                }
            }

            tx_worker_collector.send(CollectorThreadMessage::NoMoreMacs(thread_name)).unwrap();
        });

        if let Ok(thread_handle) = thread_handle_res {
            worker_thread_vector.push( WorkerThread { 
                thread_handle, 
                input_channel_tx:   tx_worker 
            } );
        }

    }

    let mut next_thread_nro: usize = 0;

    //************************************************************
    // Read the infile line by line using the lines() iterator from std::io::BufRead.
    //************************************************************
    for (_index, line) in reader.lines().enumerate() {
        let line = line.unwrap(); // Ignore errors.

        if line.trim().starts_with('#') {
            continue;
        }
        
        worker_thread_vector[next_thread_nro].input_channel_tx
            .send(WorkerThreadMessage::Line(line)).unwrap();
        next_thread_nro += 1;
        if next_thread_nro >= thread_count {
            next_thread_nro = 0;
        }
    }
    
    //************************************************************
    // Send termination message to all threads
    //************************************************************
    for thread in worker_thread_vector.iter() {
        thread.input_channel_tx
            .send(WorkerThreadMessage::NoMoreLines)
            .unwrap();
    }
    /*
    worker_thread_vector.iter().map(|thread| 
                thread.input_channel_tx
                .send(WorkerThreadMessage::NoMoreLines)
                .unwrap()
            );
    */

    let retstruct = collector_thread_handle.join().unwrap();

    print_map(&retstruct.mac_map);
    println!();
    println!("Number of measurements:           {}", retstruct.total_measurements);
    println!("Number of measurements:           {}", retstruct.invalid_measurements);
    println!("Number of invalid lines:          {}", retstruct.invalid_lines);
}

fn spawn_collector_thread(
    rx: mpsc::Receiver<CollectorThreadMessage>, 
    nro_of_workers: usize ) 
        -> thread::JoinHandle<RetMessage> {
            
    // Collector thread; this should collect MAC-address Strings 
    // and write the needed information into hash-map
    let collector_thread_handle = thread::Builder::new().name("Collector".to_string()).spawn(move || {

        let mut mac_map: BTreeMap::<String, u32> = BTreeMap::new();

        let mut total_measurements:     u32 = 0;
        let mut invalid_measurements:   u32 = 0;
        let mut invalid_lines:          u32 = 0;
        let mut threads_ready:          usize = 0;

        // for received in rx {
        loop {
            let received = rx.recv().unwrap();

            match received {
                CollectorThreadMessage::Mac(mac) => {
                    total_measurements += 1;

                    // Update counter map
                    if mac_map.contains_key(&mac) {
                        *mac_map.get_mut(&mac).unwrap() += 1;
                    }
                    else {
                        mac_map.insert( mac.clone(), 1);
                    }

                    // Filter out every line with unknown mac address
                    if !RUUVITAGS.contains_key(&mac) {
                        invalid_measurements += 1;
                        continue;
                    }
                }
                CollectorThreadMessage::Failure => { invalid_lines += 1; },
                CollectorThreadMessage::NoMoreMacs(_thread_name) => { 
                    threads_ready += 1;
                    if threads_ready == nro_of_workers {
                        break;
                    }
                },
                
            }
        }

        RetMessage {
            mac_map,
            total_measurements,
            invalid_measurements,
            invalid_lines,
        }
    });
    collector_thread_handle.unwrap()
}

fn process_line(line: &str, tx_collector: &mpsc::Sender<CollectorThreadMessage>) {

    let ilp_op: Option<influx_line_parser::Line> = influx_line_parser::Line::from_str(line);

    if let Some(line_struct) = ilp_op {
        tx_collector.send(CollectorThreadMessage::Mac(line_struct.mac().to_string())).unwrap();
    }
    else {
        tx_collector.send(CollectorThreadMessage::Failure).unwrap();
    }
}

fn print_map(mac_map: &BTreeMap::<String, u32>) {
    for (key, value) in mac_map {
        let name_op: Option<&str> = get_ruuvitag_name(key);
        let name: String = if let Some(name_) = name_op {
            name_.to_string()
        } else {
            "".to_string()
        };
        println!("{} {:20} {}", key, name, value);
    }
}

pub fn get_ruuvitag_name(keyword: &str) -> Option<&str> {
    RUUVITAGS.get(keyword).cloned()
}
