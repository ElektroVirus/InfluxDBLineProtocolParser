use std::collections::BTreeMap;
use std::fmt;

#[derive(PartialEq)]
enum Section {
    Measurement,
    TagSet,
    FieldSet,
    Timestamp
}

#[derive(PartialEq)]
enum KeyValuePairSection {
    Key,
    Value
}

pub struct Line {
    timestamp:      u64,
    measurement:    String,
    tag_set:        BTreeMap<String, String>,
    field_set:      BTreeMap<String, String>
}

impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.measurement)?;

        for key in self.tag_set.keys() {
            write!(f, ",{}={}", key, &self.tag_set[key])?;
        }

        // First space
        write!(f, " ")?;


        let mut first = true;
        for key in self.field_set.keys() {
            if ! first {
                write!(f, ",")?;
            }
            first = false;

            write!(f, "{}={}", key, &self.field_set[key])?;
        }

        // Second space
        write!(f, " ")?;

        write!(f, "{}", &self.timestamp.to_string())
    }
}

// impl fmt::Write for Line {
//     fn write_fmt<W: Write>(f: &mut W, c: char) -> Result<(), fmt::Error> {
// 
//     }
// }

#[allow(dead_code)]
impl Line {
    
    /// Returns a InfluxDB Line from the given string
    ///
    /// # Arguments
    ///
    /// * line - A string slice which holds one line of the InfluxDB LineProtocol
    ///
    /// # Note
    ///
    /// InfluxDB LineProtocol is extremely tolerant: it accepts '\' as escape 
    /// character. This implementation is incomplete: it now accepts escaped 
    /// spaces, however, it does not accept escaped '=' or escaped '\' at the 
    /// last character before a space (i.e. string 
    /// "mac=999999\\ temperature=34" is not allowed by this 
    /// implementation, althought it would be valid for InfluxDB).
    ///
    /// Please don't use spaces, \ or = in names. 
    ///
    pub fn from_str(line: &str) -> Option<Self> {

        // In which section of parsing we are
        let mut section = Section::Measurement;

        let mut measurement_str:    String = String::with_capacity(20);
        let mut tag_key_str:        String = String::with_capacity(40);
        let mut field_key_str:      String = String::with_capacity(40);
        let mut timestamp_str:      String = String::with_capacity(20);

        let mut flag_esc = false;
        let mut flag_esc_count = 0;

        for c in line.chars() {

            /*
            If current character is a backslash ('\'), enable flag_esc. 
            It must be valid for this and for second round (c1 and c2). 
            After that it must be re-enabled if the c3 is a backslash again.

            So for example with string "\\ " now the space (3. char) should
            be unescaped.
            */
            if flag_esc {
                flag_esc_count += 1;
            }
            if flag_esc_count >= 2 {
                flag_esc = false;
                flag_esc_count = 0;
            }
            if !flag_esc && c == '\\' {
                flag_esc = true;
                flag_esc_count = 0;
            }

            if c == ' ' && flag_esc {
                println!("Escaped space found");
                // Escaped spaces are not interesting
            }
            else if c == ' ' && !flag_esc {
                // println!("Unescaped space found");

                // Tag set ends in fist unescaped space
                if section == Section::TagSet {
                    section = Section::FieldSet;
                    continue;
                }

                // Field set ends in second unescaped space
                else if section == Section::FieldSet {
                    section = Section::Timestamp;
                    continue;
                }
            }
            else if c == ',' && !flag_esc {
                
                // Measurement section ends in the first comma found
                if section == Section::Measurement {
                    section = Section::TagSet;
                    continue;
                }
            }
            
            if section == Section::Measurement {
                measurement_str.push(c)
            }
            else if section == Section::TagSet {
                tag_key_str.push(c);
            }
            else if section == Section::FieldSet {
                field_key_str.push(c);
            }
            else if section == Section::Timestamp {
                timestamp_str.push(c);
            }
        }

        //*********************************************************************
        // Parse tag keys and values
        //*********************************************************************

        let mut section = KeyValuePairSection::Key;

        let mut flag_esc = false;
        let mut flag_esc_count = 0;

        let mut key:    String = String::with_capacity(10);
        let mut value:  String = String::with_capacity(10);
        let mut tag_set_tmp: BTreeMap::<String, String> = BTreeMap::new();

        for c in tag_key_str.chars() {

            if flag_esc {
                flag_esc_count += 1;
            }
            if flag_esc_count >= 2 {
                flag_esc = false;
                flag_esc_count = 0;
            }
            if !flag_esc && c == '\\' {
                flag_esc = true;
                flag_esc_count = 0;
            }

            if c == '=' && !flag_esc {
                // Key and value are delimitered using unescaped equals sign
                section = KeyValuePairSection::Value;
                continue;
            }
            else if c == ',' && !flag_esc {
                // Key-value pairs are delimitered with unescaped comma
                section = KeyValuePairSection::Key;

                tag_set_tmp.insert(
                    key.to_string(),
                    value.to_string()
                );

                key.clear();
                value.clear();

                continue;
            }

            if section == KeyValuePairSection::Key {
                key.push(c);
            }
            else if section == KeyValuePairSection::Value {
                value.push(c);
            }
        }

        // Don't forget to add the last key-value -pair
        tag_set_tmp.insert(
            key.to_string(), 
            value.to_string()
        );

        //*********************************************************************
        // Parse field keys and values
        //*********************************************************************

        let mut section = KeyValuePairSection::Key;

        let mut flag_esc = false;
        let mut flag_esc_count = 0;

        let mut key:    String = String::with_capacity(10);
        let mut value:  String = String::with_capacity(10);
        let mut field_set_tmp: BTreeMap::<String, String> = BTreeMap::new();

        for c in field_key_str.chars() {

            if flag_esc {
                flag_esc_count += 1;
            }
            if flag_esc_count >= 2 {
                flag_esc = false;
                flag_esc_count = 0;
            }
            if !flag_esc && c == '\\' {
                flag_esc = true;
                flag_esc_count = 0;
            }

            if c == '=' && !flag_esc {
                // Key and value are delimitered using unescaped equals sign
                section = KeyValuePairSection::Value;
                continue;
            }
            else if c == ',' && !flag_esc {
                // Key-value pairs are delimitered with unescaped comma
                section = KeyValuePairSection::Key;

                field_set_tmp.insert(
                    key.to_string(),
                    value.to_string()
                );

                key.clear();
                value.clear();

                continue;
            }

            if section == KeyValuePairSection::Key {
                key.push(c);
            }
            else if section == KeyValuePairSection::Value {
                value.push(c);
            }
        }

        // Don't forget to add the last key-value -pair
        field_set_tmp.insert(
            key.to_string(), 
            value.to_string()
        );

        Some(Self {
            timestamp:      timestamp_str.parse::<u64>().unwrap(),
            measurement:    measurement_str,
            tag_set:         tag_set_tmp,
            field_set:       field_set_tmp
        })
    }

    ////////////////////////////////////////////////////////////
    // Getters
    ////////////////////////////////////////////////////////////

    pub fn measurement(&self) -> &str {
        &self.measurement
    }

    pub fn timestamp(&self) -> u64 {
        self.timestamp
    }

    pub fn tag(&self, key: &str) -> &str {
        &self.tag_set[key]
    }

    pub fn field(&self, key: &str) -> &str {
        &self.field_set[key]
    }

    // RuuviTag specific functions

    pub fn mac(&self) -> &str {
        self.tag("mac")
    }

    pub fn temperature(&self) -> &str {
        self.field("temperature")
    }

    pub fn humidity(&self) -> &str {
        self.field("humidity")
    }

    pub fn pressure(&self) -> &str {
        self.field("pressure")
    }

    ////////////////////////////////////////////////////////////
    // Setters
    ////////////////////////////////////////////////////////////

    pub fn set_tag(&mut self, key: &str, value: &str) {
        self.tag_set.insert(
            key.to_string(), 
            value.to_string()
        );
    }

    pub fn validate(&self) -> bool {

        // Line protocol does not support the newline character \n in tag or 
        // field values.
        //
        // Measurement names, tag keys, and field keys cannot begin with an 
        // underscore _. The _ namespace is reserved for InfluxDB system use.

        for (key, value) in &self.tag_set {
            if value.contains('\n') {
                return false;
            }
            if key.starts_with('_') {
                return false;
            }
        }

        for (key, value) in &self.field_set {
            if value.contains('\n') {
                return false;
            }
            if key.starts_with('_') {
                return false;
            }
        }

        if self.measurement.starts_with('_') {
            return false;
        }

        true
    }
    
}

#[cfg(test)]
mod tests {

    use super::*;

///****************************************************************************
/// Measurement
///****************************************************************************

    #[test]
    fn test_m_ending_in_backslash() {
        let example_line =          String::from("ruuvi_measurements\\,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements\\,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_m_containing_space() {
        let example_line =          String::from("ruuvi\\ measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi\\ measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_m_containing_comma() {
        let example_line =          String::from("ruuvi\\,measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi\\,measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }
    
///****************************************************************************
/// Tag key
///****************************************************************************

    /// Backslash

    #[test]
    fn test_tk_escaped_backslash_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,m\\\\ac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,m\\\\ac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_escaped_backslash_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac\\\\=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac\\\\=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_escaped_backslash_front() {
        let example_line =          String::from("ruuvi_measurements,\\\\mac=E2AEAEFAA837,dataFormat=3 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,\\\\mac=E2AEAEFAA837,dataFormat=3 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    /// Space

    #[test]
    fn test_tk_escaped_space_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,ma\\ c=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,ma\\ c=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_escaped_space_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac\\ =E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac\\ =E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_escaped_space_front() {
        let example_line =          String::from("ruuvi_measurements,\\ mac=E2AEAEFAA837,dataFormat=3 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,\\ mac=E2AEAEFAA837,dataFormat=3 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }
    
    /// Equals sign

    #[test]
    fn test_tk_escaped_eq_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,ma\\=c=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,ma\\=c=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_escaped_eq_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac\\==E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac\\==E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_escaped_eq_front() {
        let example_line =          String::from("ruuvi_measurements,\\=mac=E2AEAEFAA837,dataFormat=3 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,\\=mac=E2AEAEFAA837,dataFormat=3 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_quotes() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,m\"a\"c=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,m\"a\"c=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    
///****************************************************************************
/// Tag value
///****************************************************************************

    /// Backslash

    #[test]
    fn test_tv_escaped_backslash_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAE\\\\FAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAE\\\\FAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_escaped_backslash_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837\\\\ temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837\\\\ temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_escaped_backslash_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=\\\\E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=\\\\E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    /// Space

    #[test]
    fn test_tv_escaped_space_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAE\\ FAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAE\\ FAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_escaped_space_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837\\  temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837\\  temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_escaped_space_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=\\ E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=\\ E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }
    
    /// Equals sign

    #[test]
    fn test_tv_escaped_eq_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAE\\=FAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAE\\=FAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_escaped_eq_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837\\= temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837\\= temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_escaped_eq_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=\\=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=\\=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tv_quotes() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2\"AE\"AEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2\"AE\"AEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }


///****************************************************************************
/// Field key
///****************************************************************************

    /// Backslash

    #[test]
    fn test_fk_escaped_backslash_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temp\\\\erature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temp\\\\erature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_escaped_backslash_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature\\\\=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature\\\\=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_escaped_backslash_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 \\\\temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 \\\\temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    /// Space

    #[test]
    fn test_fk_escaped_space_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temp\\ erature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temp\\ erature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_escaped_space_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature\\ =24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature\\ =24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_escaped_space_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 \\ temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 \\ temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }
    
    /// Equals sign

    #[test]
    fn test_fk_escaped_eq_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temp\\=erature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temp\\=erature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_escaped_eq_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature\\==24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature\\==24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_escaped_eq_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 \\=temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 \\=temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fk_quotes() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 te\"mp\"erature\"=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 te\"mp\"erature\"=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

///****************************************************************************
/// Field value
///****************************************************************************

    #[test]
    fn test_fv_quotes() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=2\"4.4\"6 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=2\"4.4\"6 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fv_backslash_middle() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=2\\\\4.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=2\\\\4.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fv_backslash_end() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46\\\\ 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46\\\\ 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_fv_backslash_front() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=\\\\24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=\\\\24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(example_line, example_line_result);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

///****************************************************************************
/// Timestamp
///****************************************************************************

///****************************************************************************
/// Common
///****************************************************************************

    #[test]
    fn test_to_string() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 temperature=24.46 1543795206029000000");
    
        let ilp_op: Option<Line> = Line::from_str(&example_line);
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }

    #[test]
    fn test_tk_set_tag() {
        let example_line =          String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837 humidity=3.356006433735946,pressure=98801,temperature=24.46 1543795206029000000");
        let example_line_result =   String::from("ruuvi_measurements,dataFormat=3,mac=E2AEAEFAA837,name=Mancave humidity=3.356006433735946,pressure=98801,temperature=24.46 1543795206029000000");
    
        let mut ilp_op: Option<Line> = Line::from_str(&example_line);
        if let Some(ilp) = &mut ilp_op {
            ilp.set_tag("name", "Mancave");
        }
        assert_eq!(ilp_op.unwrap().to_string(), example_line_result);
    }


}

